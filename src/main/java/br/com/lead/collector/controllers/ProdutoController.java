package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    ProdutosService produtosService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto cadastrarProdutos(@RequestBody @Valid Produto produtos){
        Produto objetoProdutos = produtosService.salvarProdutos(produtos);
        return objetoProdutos;
    }

    @GetMapping
    public Iterable<Produto> lerTodosOsProdutos (){
        return produtosService.lerTodosOsProdutos();
    }

    @GetMapping("/{id}")
    public Produto buscarPeloId (@PathVariable(name="id") int id){
        try {
            Produto produto = produtosService.buscarLeadPeloId(id);
            return produto;
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProdutos (@RequestBody @Valid Produto produtos, @PathVariable(name="id") int id){
        try{
            Produto produto = produtosService.atualizarProdutos(id, produtos);
            return produtos;
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deletarProdutos (@PathVariable(name="id") int id){
        try{
            produtosService.deletarProdutos(id);

        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, e.getMessage());
        }
    }
}
