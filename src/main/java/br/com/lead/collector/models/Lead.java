package br.com.lead.collector.models;

import javax.persistence.*;
import java.util.List;

@Entity  //identifica que é uma tabela no BD
@Table(name = "lead") //name que vai no create table
public class Lead {

    @Id //pelo java persistence marca o Id como campo de identificacao
    @GeneratedValue(strategy = GenerationType.IDENTITY) //nunca vai repetir o ID
    private int id; //chave primaria da tabela
    private String nome;
    private String cpf;
    private String email;
    private String telefone;

    @ManyToMany (cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public Lead() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
