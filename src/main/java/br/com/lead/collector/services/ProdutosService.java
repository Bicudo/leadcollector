package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutosService {

    @Autowired
    private ProdutosRepository produtosRepository;

    //criando um produto no BD
    public Produto salvarProdutos (Produto produtos){
        Produto objetoProduto = produtosRepository.save(produtos);
        return objetoProduto;
    }

    //Read do BD
    public Iterable<Produto> lerTodosOsProdutos (){
        return produtosRepository.findAll();
    }

   //Buscar no BD
   public Produto buscarLeadPeloId (int id){
        //pode retornar ou não um produto se existir no banco, caso não retorna null
       Optional<Produto> produtosOptional= produtosRepository.findById(id);

       if(produtosOptional.isPresent()){
           Produto produtos = produtosOptional.get();
           return produtos;
       }else{
           throw new RuntimeException("O produto nao foi encontrado");
       }
   }

   //Atualizar BD , ele passa o id do que vai ser atualizar e o produtos novo
    public Produto atualizarProdutos (int id, Produto produtos){
        Produto produtosDB = buscarLeadPeloId(id);
        produtos.setId(produtosDB.getId());

        return produtosRepository.save(produtos);
    }

    //Deletar do BD
    public void deletarProdutos (int id){
        if(produtosRepository.existsById(id)){
            produtosRepository.deleteById(id);
        }else{
            throw new RuntimeException("Registro não existe");
        }
    }
}
