package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTeste {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    CadastroDeLeadDTO cadastroDeLeadDTO;

    @BeforeEach
    private void setUp (){
        this.lead= new Lead();
        lead.setId(1);
        lead.setNome("Marvim");
        lead.setTelefone("11998768899");
        lead.setCpf("390.251.768-95");
        lead.setEmail("marvim@dontpanic.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setNome("GUia mochileiro da galaxia");
        produto.setDescricao("guia");

        List<Produto> produtos = new ArrayList<>();
        lead.setProdutos(produtos);

        this.cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setTelefone("1199877818191");
        cadastroDeLeadDTO.setNome("Xablau");
        cadastroDeLeadDTO.setEmail("email@gmail.com");
        cadastroDeLeadDTO.setCpf("390.251.768-95");

        List<IdProdutoDTO> idDeProdutoDTOs= new ArrayList<>();

        cadastroDeLeadDTO.setProdutos(idDeProdutoDTOs);
    }

    @Test
    public void testarBuscarPorId() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(1)).thenReturn(lead);

        //o content type indica que a comunicação com a API é via json
        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIDQueNaoExiste() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(1)).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/" + lead.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void testarLerTodosOsLeads () throws Exception {
        List<ResumoDeLeadDTO> listaLeadDTO = new ArrayList<>();

        ResumoDeLeadDTO resumoDeLeadDTO = new ResumoDeLeadDTO();
        resumoDeLeadDTO.setEmail(lead.getEmail());
        resumoDeLeadDTO.setNome(lead.getNome());
        resumoDeLeadDTO.setId(lead.getId());
        listaLeadDTO.add(resumoDeLeadDTO);

        Mockito.when(leadService.lerTodosOsLeads()).thenReturn(listaLeadDTO);
        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect((ResultMatcher) MockMvcResultMatchers.jsonPath("$"). isArray());
    }

    @Test
    public void testarCadastrarLead () throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);
        ObjectMapper objectMapper = new ObjectMapper();

        String LeadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(LeadJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Marvim")));
    }

    @Test  //testando cenario de erros nos campos
    public void testarValidacaoDeCadastroDeLead() throws Exception {
        cadastroDeLeadDTO.setCpf("3902517612");
        cadastroDeLeadDTO.setEmail("igngrasasa");

        ObjectMapper objectMapper = new ObjectMapper();

        String LeadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(LeadJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
                //.andExpect(MockMvcResultMatchers.jsonPath("$.errors[2].field", CoreMatchers.equalTo("cpf")));

        //espera que tenham feito 0 chamadas
        Mockito.verify(leadService, Mockito.times(0))
                .salvarLead(Mockito.any(CadastroDeLeadDTO.class));

    }
}
