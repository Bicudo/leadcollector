package br.com.lead.collector.controllers;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutosService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTeste {

    @MockBean
    private ProdutosService produtosService;

    @Autowired
    private MockMvc mockMvc;

    Produto produto;

    @BeforeEach
    public void setUp (){
        this.produto = new Produto();
        this.produto.setId(1);
        this.produto.setDescricao("livro de suspense");
        this.produto.setNome("O codigo Da Vinci");
    }

    @Test
    public void testarBuscarPeloId() throws Exception {
        Mockito.when(produtosService.buscarLeadPeloId(1)).thenReturn(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("O codigo Da Vinci")));

    }

    @Test
    public void testarBuscarPeloIdQueNaoExiste () throws Exception {
        Mockito.when(produtosService.buscarLeadPeloId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/" + produto.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }


}
