package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutosRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutosRepository produtosRepository;

    @Autowired //aqui cria objeto verdadeiro da Lead Service
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach //executado antes de cada método de teste
    private void setUp (){
        this.lead= new Lead();
        lead.setId(1);
        lead.setNome("Marvim");
        lead.setTelefone("11998768899");
        lead.setCpf("390.251.768-95");
        lead.setEmail("marvim@dontpanic.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setNome("GUia mochileiro da galaxia");
        produto.setDescricao("guia");

        List<Produto> produtos = new ArrayList<>();
        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscaDeLeadPeloId (){
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Lead leadObjeto = leadService.buscarLeadPeloId(8001);

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getNome(), leadObjeto.getNome());
    }

    @Test
    public void testarBuscarPeloIDQueNaoExiste (){
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarLeadPeloId(762);});
    }

    @Test
    public void testarSalvarLead (){
        CadastroDeLeadDTO cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setCpf("612.812.170-50");
        cadastroDeLeadDTO.setEmail("ingrid.bicudo@usp.br");
        cadastroDeLeadDTO.setNome("Maria Clara");
        cadastroDeLeadDTO.setTelefone("1198989898");
        IdProdutoDTO idProdutoDTO = new IdProdutoDTO();
        idProdutoDTO.setId(1);

        List<IdProdutoDTO> idDeProduto= Arrays.asList(idProdutoDTO);
        cadastroDeLeadDTO.setProdutos(idDeProduto);

        Mockito.when(produtosRepository.findAllById(Mockito.anyIterable())).thenReturn(this.produtos);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(objeto -> objeto.getArgument(0));

        Lead testeDeLead = leadService.salvarLead(cadastroDeLeadDTO);

        Assertions.assertEquals("Maria Clara", testeDeLead.getNome());
    }
}

