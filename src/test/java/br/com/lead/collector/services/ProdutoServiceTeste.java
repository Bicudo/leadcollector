package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutosRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTeste {

    @MockBean
    private ProdutosRepository produtosRepository;

    @Autowired
    ProdutosService produtosService;

    Produto produto;

    @BeforeEach
    public void setUp (){
        this.produto = new Produto();
        this.produto.setId(1);
        this.produto.setDescricao("livro de suspense");
        this.produto.setNome("O codigo Da Vinci");
    }

    @Test
    public void testarSalvarProduto (){
        Produto produto = new Produto();
        produto.setId(1);
        produto.setDescricao("Notebook dell ultima geração");
        produto.setNome("Notebool dell");
        produto.setPreco(6000);

        Mockito.when(produtosRepository.save(Mockito.any(Produto.class))).then(objeto -> objeto.getArgument(0));

        Produto produtoTeste = produtosService.salvarProdutos(produto);

        Assertions.assertEquals("Notebool dell", produtoTeste.getNome());
    }

    @Test
    public void testarBuscarPeloIdDoProduto (){
        Optional<Produto> optionalProduto = Optional.of(produto);

        Mockito.when(produtosRepository.findById(Mockito.anyInt())).thenReturn(optionalProduto);

        Produto produtoTeste = produtosService.buscarLeadPeloId(1234);

        Assertions.assertEquals(produto, produtoTeste);
        Assertions.assertEquals(produto.getNome(), produtoTeste.getNome());

    }

    @Test
    public void testarBuscarPeloIdQueNaoExiste (){
        Optional<Produto> optionalProduto = Optional.empty();

        Mockito.when(produtosRepository.findById(Mockito.anyInt())).thenReturn(optionalProduto);

        Assertions.assertThrows(RuntimeException.class, () ->  {produtosService.buscarLeadPeloId(1231);});

    }

    @Test
    public void testarAtualizarProduto (){

        Mockito.when(produtosRepository.save(Mockito.any(Produto.class))).then(objeto -> objeto.getArgument(0));

        Produto produtoTeste = produtosService.salvarProdutos(produto);

        Assertions.assertEquals(produto, produtoTeste);
        Assertions.assertEquals(produto.getNome(), produtoTeste.getNome());

    }


}
